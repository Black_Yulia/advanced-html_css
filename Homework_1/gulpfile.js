const gulp = require('gulp');
const sass = require ('gulp-sass');
const autoprefixer = require ('gulp-autoprefixer');
const clean = require ('gulp-clean');
const browserSync = require ('browser-sync').create();

const path = {
    dist: {
        html: 'dist',
        css: 'dist/css',
        img: 'dist/img',
        self: 'dist'
    },
    src : {
        html: 'src/*.html',
        scss: 'src/scss/*.scss',
        reset: 'src/scss/*.css',
        img: 'src/img/*.*'
    }
};

/*FUNCTIONS*/

const htmlBuild = () => (
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.dist.html))
        .pipe(browserSync.stream())
);

const resetBuild = () => (
    gulp.src(path.src.reset)
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
);

const imgBuild = () => (
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.dist.img))
        .pipe(browserSync.stream())
);

const scssBuild = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
);

const cleanDist = () => (
    gulp.src(path.dist.self, {allowEmpty: true})
        .pipe(clean())
);


/*WATCHERS*/

const watcher = () => (
    browserSync.init ({
        server: {
            baseDir: "./dist"
        }
    }));

gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
gulp.watch(path.src.reset, resetBuild).on('change', browserSync.reload);
gulp.watch(path.src.img, imgBuild).on('change', browserSync.reload);



/* TASKS */
gulp.task('html', htmlBuild);
gulp.task('scss', scssBuild);
gulp.task('reset', resetBuild);
gulp.task('img', imgBuild);

gulp.task('default', gulp.series (
    cleanDist,
    htmlBuild,
    scssBuild,
    resetBuild,
    imgBuild,
    watcher
));